# Language Generation

Language generation app with a full CI/CD pipeline. Includes a neural network (Transformers), backend (Flask), database (SQLAlchemy), unit tests for backend (PyTest), frontend (Vue.js), unit tests for frontend (Jest), CI/CD (Gitlab), and deployment (Heroku).

[language-generation-2022.herokuapp.com](https://language-generation-2022.herokuapp.com/)