Flask==2.0.2
Flask-Cors==3.0.10
numpy==1.22.0
--find-links https://download.pytorch.org/whl/torch_stable.html
torch==1.10.1+cpu
transformers==4.15.0
Flask-SQLAlchemy==2.5.1
pytest==6.2.5
# psycopg2-binary==2.9.3