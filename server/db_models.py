from datetime import datetime
import app


class TextGeneration(app.db.Model):
    id = app.db.Column(app.db.Integer, primary_key=True)
    input_text = app.db.Column(app.db.String, nullable=False)
    generated_text = app.db.Column(app.db.String, nullable=False)
    date_time = app.db.Column(app.db.DateTime, nullable=False, default=datetime.utcnow)

    def __init__(self, input_text, generated_text):
        self.input_text = input_text
        self.generated_text = generated_text

    def serialize(self):
        return {
            "id": self.id,
            "input_text": self.input_text,
            "generated_text": self.generated_text,
            "date_time": self.date_time,
        }
