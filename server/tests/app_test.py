from pathlib import Path

import torch
import pytest

import app

TEST_DB = "test.db"


@pytest.fixture
def client():
    BASE_DIR = Path(__file__).resolve().parent.parent
    app.app.config["TESTING"] = True
    app.app.config["DATABASE"] = BASE_DIR.joinpath(TEST_DB)
    app.app.config[
        "SQLALCHEMY_DATABASE_URI"
    ] = f"sqlite:///{BASE_DIR.joinpath(TEST_DB)}"

    app.db.create_all()
    yield app.app.test_client()
    app.db.drop_all()


def test_database(client):
    assert Path(TEST_DB).is_file()


def test_hello(client):
    torch.manual_seed(42)
    response = client.get("/hello", content_type="application/json")

    assert response.status_code == 200
    assert b"Hello" in response.data
    assert b"Hello, my name is Jai" in response.data


def test_simple_generate(client):
    torch.manual_seed(42)
    response = client.post(
        "/simple-generate",
        data='{"inputText": "Hello, today I will"}',
        content_type="application/json",
    )

    assert response.status_code == 200
    expected_responde = (
        '{"generated_text":"Hello, today I will announce that the Windows Update will be rolling out to '
        + "Linux, OS X and Windows Phone versions soon, and will provide a great deal of support for all "
        + 'Windows Phone running Windows Vista and above with Windows 10. We will find out","input_text":"Hello, '
        + 'today I will","message":"Text generated!","status":"success"}\n'
    )
    assert expected_responde.encode() == response.data


def post_generated_text(client, input_text):
    data = '{"inputText": "' + input_text + '"}'
    return client.post("/generate", data=data, content_type="application/json")


def test_generate(client):
    response = post_generated_text(client, "Hello there")

    assert response.status_code == 200
    assert b'"status":"success"' in response.data
    assert b'"message":"Text generated!"' in response.data


def test_texts(client):
    response = client.get("/texts", content_type="application/json")

    assert response.status_code == 200
    assert (
        b'{"generated_texts":[],"message":"Generated texts recovered!","status":"success"}\n'
        == response.data
    )

    post_generated_text(client, "Hello")

    response = client.get("/texts", content_type="application/json")

    assert response.status_code == 200
    assert b'"id":1' in response.data
    assert b'"id":2' not in response.data


def test_texts_page(client):
    page = 1
    response = client.get(f"/texts/{page}", content_type="application/json")

    assert response.status_code == 200
    assert (
        b'{"generated_texts":[],"message":"Generated texts at page 1 recovered!","status":"success"}\n'
        == response.data
    )

    # https://culturesconnection.com/unusual-words-in-english/
    post_generated_text(client, "This reams of financial gobbledygook")
    post_generated_text(client, "This is a scrumptious")
    post_generated_text(client, "Nature has created a wonderful serendipity")
    post_generated_text(client, "This agastopia I have for your neck")
    post_generated_text(client, "I took a post-jentacular")

    page = 1
    response = client.get(f"/texts/{page}", content_type="application/json")

    assert response.status_code == 200
    assert (
        f'"message":"Generated texts at page {page} recovered!"'.encode()
        in response.data
    )
    assert b"jentacular" in response.data
    assert b"agastopia" in response.data
    assert b'"id":5' in response.data
    assert b'"id":4' in response.data

    page = 2
    response = client.get(f"/texts/{page}", content_type="application/json")

    assert response.status_code == 200
    assert (
        f'"message":"Generated texts at page {page} recovered!"'.encode()
        in response.data
    )
    assert b"serendipity" in response.data
    assert b"scrumptious" in response.data
    assert b'"id":3' in response.data
    assert b'"id":2' in response.data

    page = 3
    response = client.get(f"/texts/{page}", content_type="application/json")

    assert response.status_code == 200
    assert (
        f'"message":"Generated texts at page {page} recovered!"'.encode()
        in response.data
    )
    assert b"gobbledygook" in response.data
    assert b'"id":1' in response.data

    page = 4
    response = client.get(f"/texts/{page}", content_type="application/json")

    assert response.status_code == 200
    assert (
        b'{"generated_texts":[],"message":"Generated texts at page 4 recovered!","status":"success"}\n'
        == response.data
    )
