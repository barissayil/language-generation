from pathlib import Path

from flask import Flask, jsonify, request
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy

from transformers import pipeline, logging


basedir = Path(__file__).resolve().parent
logging.set_verbosity_error()


# configuration
DATABASE = "flaskr.db"
# SQLALCHEMY_DATABASE_URI = os.getenv(
#   'DATABASE_URL',
#   f'sqlite:///{Path(basedir).joinpath(DATABASE)}'
# )
SQLALCHEMY_DATABASE_URI = f"sqlite:///{Path(basedir).joinpath(DATABASE)}"

# instantiate the app
app = Flask(__name__)
# load the config
app.config.from_object(__name__)
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
# init sqlalchemy
db = SQLAlchemy(app)
# enable CORS
CORS(app, resources={r"/*": {"origins": "*"}})

import db_models

generator = pipeline("text-generation", model="distilgpt2")


@app.get("/hello")
def hello():
    text = "Hello, my name is"
    text = generator(text)[0]["generated_text"]
    response = jsonify(text)
    return response


@app.post("/simple-generate")
def simple_generate():
    response_object = {"status": "success"}
    post_data = request.get_json()
    response_object["message"] = "Text generated!"
    input_text = post_data.get("inputText")
    generated_text = generator(input_text)[0]["generated_text"]
    response_object["input_text"] = input_text
    response_object["generated_text"] = generated_text
    return jsonify(response_object)


@app.post("/generate")
def generate():

    response_object = {}

    # generate text
    post_data = request.get_json()
    input_text = post_data.get("inputText")
    generated_text = generator(input_text)[0]["generated_text"]

    # commit it to db
    new_entry = db_models.TextGeneration(input_text, generated_text)
    db.session.add(new_entry)
    db.session.commit()

    # create the response
    response_object["status"] = "success"
    response_object["message"] = "Text generated!"
    response_object["input_text"] = input_text
    response_object["generated_text"] = generated_text
    response_object["date_time"] = new_entry.date_time

    return jsonify(response_object)


@app.get("/texts")
def texts():
    response_object = {}
    generated_texts = db_models.TextGeneration.query.all()
    generated_texts = [generated_text.serialize() for generated_text in generated_texts]

    response_object["status"] = "success"
    response_object["message"] = "Generated texts recovered!"
    response_object["generated_texts"] = generated_texts

    # print(response_object)

    return jsonify(response_object)


@app.get("/texts/<int:page>")
def view(page=1):
    per_page = 2
    response_object = {}

    generated_texts = db_models.TextGeneration.query.order_by(
        db_models.TextGeneration.date_time.desc()
    )
    generated_texts = generated_texts.paginate(page, per_page, error_out=False)
    generated_texts = generated_texts.items
    generated_texts = [generated_text.serialize() for generated_text in generated_texts]

    response_object["status"] = "success"
    response_object["message"] = f"Generated texts at page {page} recovered!"
    response_object["generated_texts"] = generated_texts

    return jsonify(response_object)


if __name__ == "__main__":
    app.run()
