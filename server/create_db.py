import app


# create the database and the db table
app.db.create_all()

# commit the changes
app.db.session.commit()
