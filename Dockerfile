# build
FROM node:15.7.0-alpine3.10 as build-vue
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY ./client/package*.json ./
RUN npm install
COPY ./client .
RUN npm run build

# production
FROM nginx:stable as production
WORKDIR /app
RUN apt update && \
  apt install -y bash \
    build-essential \
    git \
    curl \
    ca-certificates \
    python3 \
    python3-pip && \
  rm -rf /var/lib/apt/lists
COPY --from=build-vue /app/dist /usr/share/nginx/html
COPY ./server/requirements.txt .
RUN python3 -m pip install --no-cache-dir --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt
RUN python3 -m pip install --no-cache-dir gunicorn
COPY ./server .
COPY ./nginx/default.conf /etc/nginx/conf.d/default.conf
CMD gunicorn -b 0.0.0.0:5000 app:app --daemon && \
  sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && \
  nginx -g 'daemon off;'