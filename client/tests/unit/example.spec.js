import { shallowMount } from '@vue/test-utils';
import App from '@/App.vue';
import Browse from '@/views/Browse.vue';
import About from '@/views/About.vue';
import TextGeneration from '@/components/TextGeneration.vue';
import GeneratedText from '@/components/GeneratedText.vue';

describe('App.vue', () => {
  it('checks if the title is correct', () => {
    const wrapper = shallowMount(
      App,
      {
        stubs: ['router-link', 'router-view']
      }
    );
    const title = wrapper.get('h1')
    expect(title.text()).toBe('Language Generation');
  });
});

describe('Browse.vue', () => {
  const wrapper = shallowMount(
    Browse,
    {
      stubs: [
        'b-card-group',
        'b-button',
        'b-container',
      ]
    }
  );
  it('checks if ⬅️  is there', () => {
    const prev_button = wrapper.get('[id="prev-button"]')
    expect(prev_button.text()).toBe('⬅️');
  });
  it('checks if ➡️  is there', () => {
    const next_button = wrapper.get('[id="next-button"]')
    expect(next_button.text()).toBe('➡️');
  });
});

describe('About.vue', () => {
  const wrapper = shallowMount(About);
  it('checks if the author is there', () => {
    expect(wrapper.text()).toContain('Baris Sayil');
  });
  it('checks if the year is there', () => {
    expect(wrapper.text()).toContain('2022');
  });
});

describe('TextGeneration.vue', () => {
  const wrapper = shallowMount(
    TextGeneration,
    {
      stubs: [
        'b-form-text',
        'b-form-textarea',
        'b-button',
        'b-col',
        'b-form-invalid-feedback',
        'b-form-valid-feedback',
      ]
    }
  );
  it('checks if the helper is correct', () => {
    const helper = wrapper.get('[id="input-live-helper"]')
    expect(helper.text()).toBe('Looks good! Click the button to generate its continuation.');
  });
  it('checks if the feedback is correct', () => {
    const feedback = wrapper.get('[id="input-live-feedback"]')
    expect(feedback.text()).toBe('Your input must be between 20-100 characters long.');
  });
});

describe('GeneratedText.vue', () => {
  const inputText = 'This is';
  const generatedText = 'This is totally a generated text.';
  const wrapper = shallowMount(
    GeneratedText, 
    {
      propsData: { inputText, generatedText },
      stubs: [
        'b-card-text',
        'b-card',
      ]
    }
  );
  it('checks if input text is there', () => {
    const gt_card = wrapper.get('[id="gt-card"]')
    expect(gt_card.text()).toMatch("Input: "+ inputText);
  });
  it('checks if generated text is there', () => {
    const gt_text = wrapper.get('[id="gt-text"]')
    expect(gt_text.text()).toBe(generatedText);
  });
});
