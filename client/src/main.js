import BootstrapVue from 'bootstrap-vue';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import 'bootstrap/dist/css/bootstrap.css';

Vue.use(BootstrapVue);

Vue.config.productionTip = false;
Vue.config.silent = true;

new Vue({
  router,
  render: (h) => h(App),
}).$mount('#app');
